ECHO OFF
chcp 65001
cls
ECHO Git 4 dummies
ECHO Istruzioni
ECHO.
ECHO Ciao e benvenuto nell'esercitazione collettiva di git 4 dummies.
ECHO Prima di iniziare ogni esercizio, è bene che tu sappia come consegnare le soluzioni di alcuni esercizi.
ECHO Al termine di essi, infatti, le discuteremo tutti insieme.
ECHO.
ECHO Se questa repo è il fork della repo di esercitazione, puoi iniziare. Altrimenti, chiedici come fare!
ECHO.
ECHO Scrivi nel terminale:
ECHO.
ECHO.
ECHO primoEs_win.bat
ECHO.
ECHO.
ECHO Buon lavoro :)
